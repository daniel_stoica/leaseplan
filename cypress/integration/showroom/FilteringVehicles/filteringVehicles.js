import { onShowroomPage } from "../../../support/pageActions/showroomPage";

Given(/^I am on the Showroom page$/, () => {
  cy.visit("/en-be/business/showroom/");
  cy.clickButtonByText("Accept Cookies");
});

When(/^I click on the "([^"]*)" filter$/, (filterName) => {
  onShowroomPage.openFilter(filterName);
});

When(/^I filter vehicles by "([^"]*)"$/, (filterSelection) => {
  onShowroomPage.makeCheckBoxFilterSelection(filterSelection);
});

When(/^I click on the filters "([^"]*)" button$/, (buttonText) => {
  onShowroomPage.clickFilterButton(buttonText);
});

When(/^I click on the term & mileage filter$/, () => {
  onShowroomPage.openFilter("60 months");
});

Then(
  /^I should see ([^"]*) car models displayed as a result$/,
  (modelsCount) => {
    cy.get("[data-component=VehicleCard]")
      .should("be.visible")
      .and("have.length", modelsCount);
  }
);

Then(/^I should see ([^"]*) cars available$/, (carsCount) => {
  cy.contains("to choose from")
    .first()
    .should("have.text", `${carsCount} to choose from`);
});

When(/^I select ([^"]*) months term$/, (term) => {
  const terms = [24, 36, 48, 60];
  onShowroomPage.makeTermSliderSelection(terms.indexOf(parseInt(term)));
});
