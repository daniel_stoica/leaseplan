Feature: Filtering business lease available vehicles

  As a user, I want to be able to filter available vehicles for business lease

  Background: Navigating to Showroom Page
    Given I am on the Showroom page

  Scenario Outline: Filtering vehicles by fuel type
    When I click on the "<filterName>" filter
    And I filter vehicles by "<filterSelection>"
    And I click on the filters "Save" button
    Then I should see <carModelsCount> car models displayed as a result
    And I should see <carCount> cars available

    Examples:
      | filterName | filterSelection | carModelsCount | carCount |
      | Fuel type  | CNG             | 12             | 37       |
      | Fuel type  | Diesel          | 12             | 2108     |
      | Fuel type  | Electric        | 12             | 203      |
      | Fuel type  | Hybrid          | 12             | 480      |
      | Fuel type  | LPG             | 1              | 3        |
      | Fuel type  | Not defined     | 2              | 4        |
      | Fuel type  | Petrol          | 12             | 2413     |

  Scenario Outline: Filtering vehicles by term
    When I click on the term & mileage filter
    And I select <term> months term
    And I click on the filters "Save" button
    Then I should see <carModelsCount> car models displayed as a result
    Then I should see <carCount> cars available

    Examples:
      | term | carModelsCount | carCount |
      | 24   | 12             | 5229     |
      | 36   | 12             | 5248     |
      | 48   | 12             | 5248     |
      | 60   | 12             | 5248     |

