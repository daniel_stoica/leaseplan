class ShowroomPage {
  openFilter(filterName) {
    cy.get("div[data-component=desktop-filters]")
      .find("div")
      .contains(filterName)
      .click();
  }

  makeCheckBoxFilterSelection(filterSelection) {
    cy.get(`input[value="${filterSelection}"]`).check({ force: true });
  }

  makeTermSliderSelection(nth) {
    cy.get(".rc-slider-dot-active").eq(nth).click({ force: true });
  }

  clickFilterButton(buttonText) {
    cy.get(`[data-key="features.showroom.filters.save"]`)
      .contains(buttonText)
      .parent("button")
      .click({ force: true });
  }
}

export const onShowroomPage = new ShowroomPage();
