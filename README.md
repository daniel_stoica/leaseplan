# Personal comments from candidate

Since the assignment mentioned that I should use my imagination and there are no rules or borders, I decided to implement the tests by using a different framework, based on a tool called Cypress, instead of Webdriver. I strongly believe that using Cypress has many advantages when compared to Webdriver, so I wanted to take the opportunity to present this alternative.

Even though the main tool is different, I tried to keep the same BDD approach of writing tests, this is why the second component of this framework is Cucumber (https://github.com/TheBrainFamily/cypress-cucumber-preprocessor).

As I had limited time for this test, I only added two tests, but I hope this is good enough.
I also want to mention that even though I spent hours trying to figure this out, I could not find a solution for dealing with the range slider for prices. I am very curious to know how this can be implemented.

# Test automation framework for UI testing

The main idea of this framework is to provide easy, maintainable and reliable automated tests for UI part of the application. It was developed using Javascript ES6, Node.js, Cypress and cypress-cucumber-preprocessor.

## Framework installation

**Install node js**

download here https://nodejs.org/en/download/

**Install all dependency**

```
npm install
```

**Run all tests in CLI**

```
npm run cy:run
```

**Open Cypress Test Runner**

```
npm run cy:open
```
